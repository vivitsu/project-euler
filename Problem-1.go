/* Project Euler - Problem 1
 * 
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 *
 * Author: Vivitsu Maharaja
 * 
 * Answer: 233168
 */

package main

import "fmt"

func main() {
	var sum int = 0 
	var limit int = 1000

	for i:= 0; i < limit; i++ {
		
		if i % 3 == 0 || i % 5 == 0 {
			sum = sum + i;
		} else {
			continue;
		}
	}

	fmt.Printf("The sum of all multiples of 3 and 5 less than 1000 is %d\n", sum);
}

