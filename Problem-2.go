/* Project Euler - Problem 2
 * 
 * Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:
 *
 *	1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 *
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
 * 
 * Author: Vivitsu Maharaja
 * 
 * Usage: go run Problem-2.go -Limit=4000000
 *
 * Answer: 4613732
 */


package main

import (
	"fmt"
	"flag"
)

func main() {

	var (
		fibPrevPrev int64 = 1;
		fibPrev int64 = 1;
		fibCurr int64 = 0;
		limit int64;
		sum int64;
	)

	flag.Int64Var(&limit, "Limit", 4000000, "The Nth term in the Fibonnaci series");
	flag.Parse();

	for {

		fibCurr = fibPrev + fibPrevPrev;
		fibPrevPrev = fibPrev;
		fibPrev = fibCurr;

		if fibCurr > limit {
			break;
		}

		if fibCurr % 2 == 0 {

			sum = sum + fibCurr;
		}
	}

	fmt.Printf("The sum of all even-valued terms less than 4 million in the Fibonacci series is %d\n", sum);

}

